package Snake;

import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.Timer;


public class GameBoard extends JPanel implements KeyListener, ActionListener
{
    public static int realWidth;
    public static int realHeight;
    public static final int OBJECT_SIZE = 30;

    private Game game;
    private Map<GameObjectType, BufferedImage> images;
    private BufferedImage ground;
    private BufferedImage snakeface;
    private BufferedImage body;
    private BufferedImage youwin;
    private static final String GAME_MESSAGE = "Try to get 200 points!";
    private static final String LOSE_MESSAGE = "You lost! Try again";
    private static final String SCORE_MESSAGE = "/200";
    private String currentMessage;
    private Timer timer;
    private static final int DELAY = 140;

    public GameBoard(Game game)
    {
        this.game = game;
        realWidth = game.getFieldSize().getWidth() * OBJECT_SIZE;
        realHeight = game.getFieldSize().getHeight() * OBJECT_SIZE + 70;
        setPreferredSize(new Dimension(realWidth, realHeight));
        setBackground(Color.black);
        setFocusable(true);
        requestFocus();
        addKeyListener(this);
        currentMessage = GAME_MESSAGE;
        loadImages();
        timer = new Timer(DELAY, this);
        timer.start();
    }

    private void loadImages()
    {
        BufferedImage wall;
        BufferedImage apple;
        BufferedImage strawberry;
        BufferedImage pill;
        BufferedImage portal;
        BufferedImage stone;
        try
        {
            apple = ImageIO.read(new File("src/Snake/pics/apple.png"));
            body = ImageIO.read(new File("src/Snake/pics/body.svg"));
            stone = ImageIO.read(new File("src/Snake/pics/stone.png"));
            ground = ImageIO.read(new File("src/Snake/pics/ground.png"));
            pill = ImageIO.read(new File("src/Snake/pics/pill.png"));
            portal = ImageIO.read(new File("src/Snake/pics/portal.png"));
            snakeface = ImageIO.read(new File("src/Snake/pics/snakeface.png"));
            strawberry = ImageIO.read(new File("src/Snake/pics/strawberry.gif"));
            wall = ImageIO.read(new File("src/Snake/pics/wall.png"));
            youwin = ImageIO.read(new File("src/Snake/pics/youwin.png"));
            images = new HashMap<GameObjectType, BufferedImage>();
            images.put(GameObjectType.Apple, apple);
            images.put(GameObjectType.Pill, pill);
            images.put(GameObjectType.Portal, portal);
            images.put(GameObjectType.Strawberry, strawberry);
            images.put(GameObjectType.Wall, wall);
            images.put(GameObjectType.Stone, stone);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void drawImage(Graphics g, Image image, Point point)
    {
        g.drawImage(image, point.getX()*OBJECT_SIZE, point.getY()*OBJECT_SIZE, null);
    }

    public void paint(Graphics g)
    {
        super.paint(g);
        for (int i = 0; i < game.getFieldSize().getWidth(); i++)
            for (int j = 0; j < game.getFieldSize().getHeight(); j++)
                drawImage(g, ground, new Point(i, j));
        for (Point point : game.getSnake().getBody())
        {
            drawImage(g, body, point);
        }
        drawImage(g, snakeface, new Point(game.getSnake().getHead().getX(), game.getSnake().getHead().getY()));
        for (GameObject gameObject : game.getObjects())
        {
            drawImage(g, images.get(gameObject.getGameObjectType()),
                    new Point(gameObject.getLocation().getX(), gameObject.getLocation().getY()));
        }
        Graphics2D g2 = (Graphics2D) g;
        Font font = new Font("Serif", Font.PLAIN, 30);
        g2.setFont(font);
        g.setColor(Color.GREEN);
        g.drawString(currentMessage, 25, realHeight - 15);
        g.drawString(game.getSnake().getScore() + SCORE_MESSAGE, 25, realHeight - 45);
        if (game.getSnake().getScore() >= 200)
        {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, realWidth, realHeight);
            g.drawImage(youwin, realWidth/4, 0, null);
            timer.stop();
            timer.restart();
            game.update();
            currentMessage = GAME_MESSAGE;
        }
    }

    private void gameOver()
    {
        currentMessage = LOSE_MESSAGE;
        game.restart();
    }

    public void keyPressed(KeyEvent e)
    {
        currentMessage = GAME_MESSAGE;
        int key = e.getKeyCode();
        setDirection(key);
        repaint();
    }

    public void setDirection(int key)
    {
        switch (key)
        {
            case KeyEvent.VK_LEFT:
                game.setSnakeDirection(Direction.Left);
                break;
            case KeyEvent.VK_RIGHT:
                game.setSnakeDirection(Direction.Right);
                break;
            case KeyEvent.VK_UP:
                game.setSnakeDirection(Direction.Up);
                break;
            case KeyEvent.VK_DOWN:
                game.setSnakeDirection(Direction.Down);
                break;
            default:
                break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (game.getSnake().getIsAlive())
        {
            game.update();
        }
        else
        {
            gameOver();
        }
        repaint();
    }

    public void keyReleased(KeyEvent arg0)
    {
        // TODO Auto-generated method stub
    }

    public void keyTyped(KeyEvent arg0)
    {
        // TODO Auto-generated method stub
    }
}
