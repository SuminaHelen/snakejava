package Snake;

public enum Direction
{
    Up(new Point(0, -1)),
    Down(new Point(0, 1)),
    Left(new Point(-1, 0)),
    Right(new Point(1, 0)),
    None(new Point(0, 0));

    private Point offset;
    private Direction opposite;

    static
    {
        Up.opposite = Direction.Down;
        Down.opposite = Direction.Up;
        Right.opposite = Direction.Left;
        Left.opposite = Direction.Right;
    }

    Direction(Point offset)
    {
        this.offset = offset;
    }

    public Point getOffset()
    {
        return this.offset;
    }

    public Direction getOpposite()
    {
        return this.opposite;
    }
}
