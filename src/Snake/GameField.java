package Snake;

import java.util.Random;
import java.util.function.Predicate;

public class GameField
{
    private int width;
    private int height;

    public GameField(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public Point controlBorders(int x, int y, Point point)
    {
        int targetX = x;
        int targetY = y;
        if (x >= getWidth())
        {
            targetX = 0;
            targetY = point.getY();
        }
        if (x < 0)
        {
            targetX = getWidth() - 1;
            targetY = point.getY();
        }
        if (y >= getHeight())
        {
            targetX = point.getX();
            targetY = 0;
        }
        if (y < 0)
        {
            targetX = point.getX();
            targetY = getHeight() - 1;
        }
        return new Point(targetX, targetY);
    }

    public Point findFreePoint(Predicate<Point> checkCollision)
    {
        Random random = new Random();
        Point randomPoint;
        while (true)
        {
            randomPoint = new Point(random.nextInt(width), random.nextInt(height));
            boolean collision = checkCollision.test(randomPoint);
            if (!collision)
                break;
        }
        return randomPoint;
    }
}