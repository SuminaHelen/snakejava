package Snake;

import javax.swing.*;
import java.awt.*;

public class Main
{
    private static Game initGame()
    {
        return new Game(new GameField(40, 20));
    }

    public static void main(String[] args)
    {
        Game game = initGame();
        JFrame frame = new JFrame("SNAKE");
        frame.setContentPane(new GameBoard(game));
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(GameBoard.realWidth, GameBoard.realHeight));
        frame.setVisible(true);
    }
}

// ControlBorders теперь в GameField
// controlAppleQuantity вызывается в update (и только в нем если я не ошибаюсь)
// initLevel в Game теперь без аргументов, наверное это надо было сделать, хз, и приватный
// addObject разбила и перенесла часть в GameField, выглядит странновато но ладно
// убрала removeObjects
// в GameObjectType сделала геттеры, исправила MakeChanges

/*
public void paint(Graphics g)
        Нужно сделать обёртку для g.drawImage, которая будет переводить внутриигровые координаты в реальные, а не просто функцию умножения
*/
