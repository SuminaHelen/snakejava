package Snake;

import com.sun.javafx.image.BytePixelSetter;

import java.util.*;
import java.util.function.Predicate;

public class Game
{
    private Snake snake;
    private GameField fieldSize;
    private List<GameObject> objects;
    private Map<Point, Point> portals;

    public void update()
    {
        if (!snake.getIsAlive())
            return;

        Point target = getTarget();
        if (snake.getBody().contains(target) && !snake.getBody().getLast().equals(target))
        {
            snake.setIsAlive(false);
            return;
        }
        Optional<GameObject> objectToRemove = Optional.empty();
        for (GameObject gameObject : objects)
        {
            boolean willTouch = target.equals(gameObject.getLocation());
            if (willTouch)
            {
                if (gameObject.getGameObjectType() == GameObjectType.Portal)
                {
                    Point exit = portals.get(gameObject.getLocation());
                    target = exit;
                }
                else {
                    gameObject.makeChanges(this, target);
                    objectToRemove = Optional.of(gameObject);
                }
                break;
            }
        }
        if (!snake.getIsAlive())
            return;
        if (objectToRemove.isPresent())
            objects.remove(objectToRemove.get());
        else {
            snake.move(target);
        }
        if (snake.getLength() + objects.size() < fieldSize.getWidth() * fieldSize.getHeight())
            controlAppleQuantity();
    }

    private Point getTarget()
    {
        int dx = snake.getDirection().getOffset().getX();
        int dy = snake.getDirection().getOffset().getY();
        int snakeX = snake.getHead().getX();
        int snakeY = snake.getHead().getY();
        return fieldSize.controlBorders(snakeX + dx, snakeY + dy, snake.getHead());
    }

    public Game(Snake snake, GameField fieldSize)
    {
        this.snake = snake;
        this.objects = new ArrayList<GameObject>();
        this.portals = new HashMap<Point, Point>();
        this.fieldSize = fieldSize;
    }

    public Game(GameField fieldSize)
    {
        this(null, fieldSize);
        initLevel();
    }

    public Snake getSnake()
    {
        return snake;
    }

    public void addObject(GameObject object)
    {
        objects.add(object);
    }

    public List<GameObject> getObjects()
    {
        return objects;
    }

    public GameField getFieldSize()
    {
        return fieldSize;
    }

    public void addObject(GameObjectType objectType)
    {
        Predicate<Point> checkCollision = (randomPoint) -> getSnake().getBody().contains(randomPoint) ||
                getObjects().stream().anyMatch((o) -> o.getLocation().equals(randomPoint));
        Point newPoint = fieldSize.findFreePoint(checkCollision);
        this.addObject(new GameObject(newPoint, objectType));
        if (objectType == GameObjectType.Portal)
        {
            Point newPoint2 = fieldSize.findFreePoint(checkCollision);
            portals.put(newPoint, newPoint2);
            portals.put(newPoint2, newPoint);
            this.addObject(new GameObject(newPoint2, objectType));
        }
    }

    private void initLevel()
    {
        Deque<Point> snake_body = new ArrayDeque<Point>();
        snake_body.addFirst(new Point(1, 1));
        snake = new Snake(snake_body, 0, Direction.None);
        for (int i = 0; i < fieldSize.getWidth(); i++)
            for (int j = 0; j < fieldSize.getHeight(); j++)
            {
                if ((i == 0 || j == 0 || i == fieldSize.getWidth() - 1 || j == fieldSize.getHeight() - 1) && i != 5)
                    addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
            }
        addObject(GameObjectType.Apple);
        addObject(GameObjectType.Strawberry);
        addObject(GameObjectType.Pill);
        for (int i = 0; i < 15; i++)
            addObject(GameObjectType.Stone);
        addObject(GameObjectType.Portal);
    }

    public void controlAppleQuantity()
    {
        long apples = getObjects().stream()
                .filter(x -> x.getGameObjectType() == GameObjectType.Apple)
                .count();
        if (apples == 0)
            addObject(GameObjectType.Apple);
    }

    public void restart()
    {
        objects.clear();
        initLevel();
    }

    public void setSnakeDirection(Direction direction)
    {
        snake.setDirection(direction);
    }

}