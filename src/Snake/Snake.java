package Snake;

import java.util.Deque;

public class Snake
{
    private Deque<Point> body;
    private boolean isAlive;
    private int score;
    private Direction direction;

    public Snake(Deque<Point> body, int score, Direction direction)
    {
        this.body = body;
        this.isAlive = true;
        this.score = score;
        this.direction = direction;
    }

    public Point getHead()
    {
        return body.getFirst();
    }

    public int getLength()
    {
        return body.size();
    }

    public Deque<Point> getBody()
    {
        return body;
    }

    public void increase(Point head)
    {
        body.addFirst(head);
    }


    public void decrease()
    {
        body.removeLast();
        if (body.size() == 0)
            setIsAlive(false);
    }

    public boolean getIsAlive()
    {
        return isAlive;
    }

    public void setIsAlive(boolean isAlive)
    {
        this.isAlive = isAlive;
    }

    public int getScore()
    {
        return score;
    }


    public void addScore(int score)
    {
        this.score += score;
    }

    public Direction getDirection()
    {
        return direction;
    }

    public void setDirection(Direction newDirection)
    {
        if (newDirection == getDirection().getOpposite())
            return;
        direction = newDirection;
    }

    public void move(Point target)
    {
        body.removeLast();
        body.addFirst(target);
    }
}
