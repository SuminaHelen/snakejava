package Snake;

public class GameObject
{
    public GameObjectType getGameObjectType()
    {
        return type;
    }

    public Point getLocation()
    {
        return location;
    }

    private GameObjectType type;
    private Point location;

    public GameObject(Point location, GameObjectType gameObjectType)
    {
        this.location = location;
        this.type = gameObjectType;
    }

    @Override
    public String toString() {
        return "GameObject{" +
                "type=" + type +
                ", location=" + location +
                '}';
    }

    public void makeChanges(Game game, Point target)
    {
        Snake snake = game.getSnake();
        snake.addScore(type.getScores());
        if (type.isDeath())
        {
            snake.setIsAlive(false);
            return;
        }
        if (type.isIncrease())
            snake.increase(target);
        else
            snake.move(target);
        if (type.isDecrease())
            snake.decrease();
    }
}